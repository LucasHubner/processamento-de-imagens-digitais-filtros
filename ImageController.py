import gi
gi.require_version('Gtk','3.0')
from gi.repository import Gtk
import cv2
from matplotlib.figure import Figure


class ImageController:
    def __init__(self):
        self.mainImage = None
        self.resultImage = None
################################################################################
    def readImage(self, filePath):
        self.filePath = filePath
        img = cv2.imread(filePath, cv2.IMREAD_UNCHANGED)

        return img
################################################################################
    def getFigure(self, img, title):
        figure = Figure()
        ax = figure.add_subplot(111)
        try:
            # Mostra figure no canvas e seta origin como Lower (Pode inverter a imagem em alguns sistemas)
            ax.imshow(cv2.cvtColor(img,cv2.COLOR_BGR2RGB), origin='lower')
        except Exception as e:
            print(e)
            ax.imshow(img,'gray', origin='lower')
        ax.autoscale(False)
        ax.set_title(title)
        ax.set_xticks([])
        ax.set_yticks([])
        return figure
################################################################################
