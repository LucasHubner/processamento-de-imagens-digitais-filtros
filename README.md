## Requerimentos ##
* Linux
* Gtk 3

## Python Libs ##
* matplotlib == 1.5.1
* numpy == 1.11.0
* cv2 == 1.0
* gi == 1.2

## Filtros Implementados ##

### Suavização ###
  * Media
  * Mediana

### Noises ###
  * Salt & Pepper
  * Gaussian
  * Poisson
  * Specke

### Outros ###
  * Roberts
  * Prewitt Operator
  * Alto Reforço
  * Sobel

### Colors ###
  * Invert Colors
  * Rgb2Gray
  * Binarize

### Frequencia ###
  * Passa Alta
  * Passa Baixa
  * Equalizar Histograma