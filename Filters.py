import cv2
import copy
import numpy

class ImageFilter:
    def __init__(self):
        self.image = None
################################################################################
    def grayScale(self, img):
        try:
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        except Exception as e:
            print(e)
            return img
        return gray
################################################################################
    def saltPepper(self, img):
        image_dimension = list(img.shape)[:2]
        s_vs_p = 0.5
        amount = 0.004
        noised_image = copy.copy(img)

        # Salt mode
        num_salt = numpy.ceil(amount * img.size * s_vs_p)
        coords = [numpy.random.randint(0, i - 1, int(num_salt)) for i in image_dimension]
        noised_image[coords] = 255

        # Pepper mode
        num_pepper = numpy.ceil(amount * img.size * (1. - s_vs_p))
        coords = [numpy.random.randint(0, i - 1, int(num_pepper)) for i in image_dimension]
        noised_image[coords] = 0

        return noised_image
################################################################################

    def applyFilter(self, mask):
        # Converte mask para array compativel
        mask = numpy.asanyarray(mask,numpy.float32)
        # Aplica mascara
        return cv2.filter2D(self.image,-1, mask)
################################################################################

    def passaAlta(self, img):
        self.image = img
        mask = [[-1,-1,-1],[-1,8,-1],[-1,-1,-1]]
        return img + self.applyFilter(mask)
################################################################################

    def passaBaixa(self,img):
        self.image = img
        mask =[[2/9,2/9,2/9],[2/9,2/9,2/9], [2/9,2/9,2/9]]
        return  self.applyFilter(mask)
################################################################################

    def sobel(self, img):
        self.img = img
        ddepth = cv2.CV_64F

        sobel_x = cv2.Sobel(img, ddepth, 1, 0)
        sobel_y = cv2.Sobel(img, ddepth, 0, 1)

        sobel_x = cv2.convertScaleAbs(sobel_x)
        sobel_y = cv2.convertScaleAbs(sobel_y)

        return img + cv2.addWeighted(sobel_x, 0.5, sobel_y, 0.5, 0)
################################################################################

    def prewitt(self, img):
        self.img = img
        mask_x = [[-1,-1,-1],[0,0,0],[1,1,1]]
        mask_y = [[-1,0,1],[-1,0,1],[-1,0,1]]

        prewitt_x = self.applyFilter(mask_x)
        prewitt_y = self.applyFilter(mask_y)

        return img + cv2.addWeighted(prewitt_x, 0.5, prewitt_y, 0.5, 0)
################################################################################
    def roberts(self, img):
        self.img = img
        mask_x = [[1,0],[0,-1]]
        mask_y = [[0,1],[-1,0]]

        roberts_x = self.applyFilter(mask_x)
        roberts_y = self.applyFilter(mask_y)

        return img + cv2.addWeighted(roberts_x, 0.5, roberts_y, 0.5, 0)
################################################################################
    def media(self, img, mask_size):
        return cv2.blur(img, (mask_size, mask_size))

################################################################################
    def mediana(self, img, mask_size):
        # Se o tamanho da mascara não for par, adicione 1
        if(mask_size % 2 == 0):
            mask_size += 1
        return cv2.medianBlur(img, mask_size)

################################################################################
    def speckle(self, img):
        row,col,ch = img.shape
        gauss = numpy.random.randn(row,col,ch)
        gauss = gauss.reshape(row,col,ch)
        noisy = img + img * gauss
        return noisy;

################################################################################
    def gaussian(self, img):
        row,col,ch= img.shape
        mean = 0
        var = 0.1
        sigma = var**0.5
        gauss = numpy.random.normal(mean,sigma,(row,col,ch))
        gauss = gauss.reshape(row,col,ch)
        noisy = img + gauss
        return noisy

################################################################################
    def poisson(self, img):
      vals = len(numpy.unique(img))
      vals = 2 ** numpy.ceil(numpy.log2(vals))
      noisy = numpy.random.poisson(img * vals) / float(vals)
      return noisy
################################################################################
    def invert(self, img):
        return numpy.invert(img)
################################################################################
    def binarize(self, img, threshold_value, is_adaptive):
        gray_image = self.grayScale(img)
        if is_adaptive:
            self.resultImg = cv2.adaptiveThreshold(gray_image, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
            output_value = 'Adaptive'
        else:
            ret, self.resultImg = cv2.threshold(gray_image,threshold_value,255,cv2.THRESH_BINARY)
            output_value = threshold_value
        return self.resultImg
################################################################################
    def grayThresh(self, img, threshold_value, is_adaptive):
        gray_image = self.grayScale(img)

        bin_image = self.binarize(img, threshold_value,is_adaptive)
        return cv2.threshold ( gray_image, bin_image, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU );
################################################################################
    def equalizeHist(self, img):
        return cv2.equalizeHist(img)

    def altoReforco(self,img):
        newimg= (10 -1) * img +self.passaAlta(img)
        high = self.passaAlta(img)
        return img + high
