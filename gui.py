import gi
gi.require_version('Gtk','3.0')
from gi.repository import Gtk
from matplotlib.figure import Figure
from ImageController import ImageController
from matplotlib.backends.backend_gtk3cairo import FigureCanvasGTK3Cairo as FigureCanvas

from Filters import ImageFilter
from mask_dialog import MaskDialog
from threshold_dialog import ThresholdDialog
import numpy
import cv2

class MainWindow:
    def __init__(self):
        # Setando variaveis da classe
        self.mainCanvas = None
        self.filterCanvas = None
        self.mainImg = None
        self.filterImg = None
        self.imageFilter = ImageFilter()
        self.initialized = False
        self.imageController = ImageController()

        # Inicia o Gtk e seta layout
        self.builder = Gtk.Builder()
        self.builder.add_from_file('mainwindow.glade')
        self.builder.connect_signals(self)

        # Captura objetos necessários e seta tamanho
        self.box = self.builder.get_object("innerBox")
        self.window = self.builder.get_object("mainWindow")
        self.window.set_default_size(800,600)
        self.window.maximize()
        self.window.show_all()

#-----------------------------------------------------------------------------#
    def setMainImage(self, img):
        # Seta variavel de classe, cria figura e remove imagem anterior
        self.mainImg = img
        figure = self.imageController.getFigure(img,"Original")

        # Seta figura e exibe na tela
        self.mainCanvas = FigureCanvas(figure)
        self.box.pack_start(self.mainCanvas,True,True, 0)
        self.window.show_all()
#-----------------------------------------------------------------------------#
    def setFilterImage(self,img):
        self.filterImg = img
        figure = self.imageController.getFigure(img,"Modificada")

        if(self.filterCanvas):
            self.box.remove(self.filterCanvas)
        # Seta figura e exibe na tela
        self.filterCanvas = FigureCanvas(figure)
        self.box.pack_end(self.filterCanvas,True,True, 0)
        self.window.show_all()
#-----------------------------------------------------------------------------#
    def initialize(self, filePath):
        self.initialized = True
        img = self.imageController.readImage(filePath)
        figure = self.imageController.getFigure(img,"Original")
        self.mainCanvas = FigureCanvas(figure)
        self.box.pack_start(self.mainCanvas, True,True,0)
        self.window.show_all()
#-----------------------------------------------------------------------------#
    def removeImages(self):
        if(self.mainCanvas):
            self.box.remove(self.mainCanvas)
        if(self.filterCanvas):
            self.box.remove(self.filterCanvas)

################################################################################
#Screnn Handlers
################################################################################
#Menu Arquivo
    def onMenuOpen(self, widget):
        fileName = None

        # Criando Dialog para selecionar arquivo
        dialog = Gtk.FileChooserDialog("Please choose a file", self.window,
                Gtk.FileChooserAction.OPEN,
                (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                 Gtk.STOCK_OPEN, Gtk.ResponseType.OK))

        # Adiciona filtro ao dialog para mostrar apenas imagens
        dialogFilters = Gtk.FileFilter()
        dialogFilters.set_name("Image files")
        dialogFilters.add_mime_type("image/*")

        # Adiciona filtro ao dialog
        dialog.add_filter(dialogFilters)

        response = dialog.run()

        #  Se o arquivo for selecionado, então seta fileName
        if response == Gtk.ResponseType.OK:
            fileName = dialog.get_filename()
        dialog.destroy()

        if (fileName):
            if(self.initialized == False):
                self.initialize(fileName)

            img = self.imageController.readImage(fileName)
            self.removeImages()
            self.setMainImage(img)
            self.setFilterImage(img)
            self.setFilterImage(self.imageFilter.passaBaixa(self.filterImg))
            self.setFilterImage(self.mainImg)

#-----------------------------------------------------------------------------#
    def onRemoveFilters(self, widget):
        self.setFilterImage(self.mainImg)
#-----------------------------------------------------------------------------#
    def onDeleteWindow(self, *args):
        Gtk.main_quit(*args)

    def onSaveImage(self, widget):
        self.saveImplement()

    def saveImplement(self):
        dialog = Gtk.FileChooserDialog("Salvar Arquivo", self.window,
                Gtk.FileChooserAction.SAVE,
                (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                 Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
        response = dialog.run()

        #  Se o arquivo for selecionado, então seta fileName
        if response == Gtk.ResponseType.OK:
            fileName = dialog.get_filename()
        dialog.destroy()

        if (fileName):
            cv2.imwrite(fileName, self.filterImg)


################################################################################
#Menu Suavização
################################################################################
    def onMenuMediana(self, widget):
        self.mask_dialog = MaskDialog(self.window)
        mask_value = self.mask_dialog.open_dialog()
        if(mask_value != None):
            resulted_figure = self.imageFilter.mediana(self.filterImg, mask_value)
            self.setFilterImage(resulted_figure)

#-----------------------------------------------------------------------------#
    def onMenuMedia(self, widget):
        self.mask_dialog = MaskDialog(self.window)
        mask_value = self.mask_dialog.open_dialog()
        if(mask_value != None):
            resultImg = self.imageFilter.media(self.filterImg, mask_value)
            self.setFilterImage(resultImg)

################################################################################
#Menu Outros
################################################################################
    def onMenuSobel(self, widget):
        self.setFilterImage(self.imageFilter.sobel(self.filterImg))

#-----------------------------------------------------------------------------#
    def onMenuAltoReforco(self, widget):
        self.setFilterImage(self.imageFilter.altoReforco(self.filterImg))

#-----------------------------------------------------------------------------#
    def onMenuPrewitt(self, widget):
        self.setFilterImage(self.imageFilter.prewitt(self.filterImg))

#-----------------------------------------------------------------------------#
    def onMenuRoberts(self, widget):
        self.setFilterImage(self.imageFilter.roberts(self.filterImg))

#-----------------------------------------------------------------------------#
    def onMenuSeamCarving(self, widget):
        self.notImplemented()


################################################################################
#Menu Noises
################################################################################
    def onMenuSpeckle(self, widget):
        self.setFilterImage(self.imageFilter.speckle(self.filterImg))


#-----------------------------------------------------------------------------#
    def onMenuSaltPepper(self, widget):
        self.setFilterImage(self.imageFilter.saltPepper(self.filterImg))

#-----------------------------------------------------------------------------#
    def onMenuPoisson(self, widget):
        self.setFilterImage(self.imageFilter.poisson(self.filterImg))


#-----------------------------------------------------------------------------#
    def onMenuLocalvar(self, widget):
        self.notImplemented()

#-----------------------------------------------------------------------------#
    def onMenuGaussian(self, widget):
        self.setFilterImage(self.imageFilter.gaussian(self.filterImg))

#-----------------------------------------------------------------------------#
#Menu Color
    def onMenuRgb2Gray(self, widget):
        self.setFilterImage(self.imageFilter.grayScale(self.filterImg))

#-----------------------------------------------------------------------------#
    def onMenuBinarizeImage(self, widget):
        self.threshold_dialog = ThresholdDialog(self.window)
        threshold_value, is_adaptive = self.threshold_dialog.open_dialog()
        if(threshold_value != None):
            resulted_figure = self.imageFilter.binarize(self.filterImg,threshold_value, is_adaptive)
            self.setFilterImage(resulted_figure)


#-----------------------------------------------------------------------------#
    def onMenuInvertColors(self, widget):
        self.setFilterImage(self.imageFilter.invert(self.filterImg))


################################################################################
#Menu Frequencia
################################################################################
    def onMenuPassaBaixa(self, widget):
        self.setFilterImage(self.imageFilter.passaBaixa(self.filterImg))

#-----------------------------------------------------------------------------#
    def onMenuPassaAlta(self, widget):
        self.setFilterImage(self.imageFilter.passaAlta(self.filterImg))
#-----------------------------------------------------------------------------#
    def onEqualizeHist(self, widget):
        self.setFilterImage(self.imageFilter.equalizeHist(self.filterImg))

################################################################################
#Menu Ajuda
################################################################################
    def onMenuSobre(self, widget):
        aboutdialog = Gtk.AboutDialog()

        # lists of authors and documenters (will be used later)
        authors = ["Lucas Guilherme Hübner"]
        documenters = ["Lucas Guilherme Hübner"]

        # we fill in the aboutdialog
        aboutdialog.set_program_name("About")
        aboutdialog.set_copyright("Copyright \xa9 2012 Lucas Guilherme Hübner")
        aboutdialog.set_authors(authors)
        aboutdialog.set_documenters(documenters)
        aboutdialog.set_website("https://bitbucket.org/LucasHubner/processamento-de-imagens-digitais-filtros/overview")
        aboutdialog.set_website_label("Fork on BitBucket")

        # we do not want to show the title, which by default would be "About AboutDialog Example"
        # we have to reset the title of the messagedialog window after setting
        # the program name
        aboutdialog.set_title("About")

        # show the aboutdialog
        aboutdialog.show()


#################################################################
# End of the screen handlers
################################################################
    def notImplemented(self):
        messagedialog = Gtk.MessageDialog(parent=self.window,
                                          flags=Gtk.DialogFlags.MODAL,
                                          type=Gtk.MessageType.WARNING,
                                          buttons=Gtk.ButtonsType.OK,
                                          message_format="Função não implementada")
        # show the messagedialog
        response = messagedialog.run()

        messagedialog.destroy()

################################################################################

if __name__ == '__main__':
    mv = MainWindow()
    Gtk.main()
